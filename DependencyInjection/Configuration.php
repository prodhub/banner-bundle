<?php

namespace ADW\BannerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('adw_banner');

        $rootNode
            ->children()
                ->arrayNode('contexts')
                    ->useAttributeAsKey('id', false)
                    ->prototype('array')
                        ->children()
                            ->scalarNode('id')->isRequired()->cannotBeEmpty()->end()
                            ->scalarNode('name')->end()
                            ->scalarNode('template')->defaultValue('ADWBannerBundle::default_block.html.twig')->end()
                        ->end()
                    ->end()
                ->end()
                ->scalarNode('default_template')->defaultValue('ADWBannerBundle::default_block.html.twig')->end()
                ->scalarNode('default_not_found_template')->defaultValue('ADWBannerBundle::not_found_banner.html.twig')->end()
            ->end();

        return $treeBuilder;
    }
}

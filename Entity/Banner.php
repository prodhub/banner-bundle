<?php

namespace ADW\BannerBundle\Entity;

use ADW\CommonBundle\Traits\AdvancedPublishableTrait;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Discriminator;
use Sonata\MediaBundle\Model\Media;

/**
 * Class AbstractBanner
 *
 * @author Artur Vesker
 *
 * @ORM\Entity(repositoryClass="ADW\BannerBundle\Repository\DoctrineBannerRepository")
 * @ORM\Table(name="banner")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"image_banner" = "ImageBanner", "html_banner" = "HTMLBanner"})
 */
abstract class Banner
{

    const TYPE_IMAGE = 'image';
    const TYPE_HTML = 'html';

    /**
     * @var string
     *
     * @ORM\Id()
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $context;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    protected $weight = 0;

    /**
     * @var string
     *
     * Discriminator Field
     */
    protected $type;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $published = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    protected $publicationStartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    protected $publicationEndDate;

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set context
     *
     * @param string $context
     *
     * @return self
     */
    public function setContext($context)
    {
        $this->context = $context;

        return $this;
    }

    /**
     * Get context
     *
     * @return string
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return self
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return \DateTime
     */
    public function getPublicationStartDate()
    {
        return $this->publicationStartDate;
    }

    /**
     * @param \DateTime $publicationStartDate
     * @return self
     */
    public function setPublicationStartDate($publicationStartDate)
    {
        $this->publicationStartDate = $publicationStartDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPublicationEndDate()
    {
        return $this->publicationEndDate;
    }

    /**
     * @param \DateTime $publicationEndDate
     * @return self
     */
    public function setPublicationEndDate($publicationEndDate)
    {
        $this->publicationEndDate = $publicationEndDate;

        return $this;
    }

    /**
     * @param boolean $published
     * @return self
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

}

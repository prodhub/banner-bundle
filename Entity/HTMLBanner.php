<?php

namespace ADW\BannerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class HTMLBanner
 *
 * @package ADW\BannerBundle\Entity
 * @author Artur Vesker
 *
 * @ORM\Entity(repositoryClass="ADW\BannerBundle\Repository\DoctrineBannerRepository")
 */
class HTMLBanner extends Banner
{

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    protected $html;

    /**
     * Set html
     *
     * @param string $html
     *
     * @return self
     */
    public function setHtml($html)
    {
        $this->html = $html;

        return $this;
    }

    /**
     * Get html
     *
     * @return string
     */
    public function getHtml()
    {
        return $this->html;
    }

}
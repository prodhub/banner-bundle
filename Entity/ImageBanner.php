<?php

namespace ADW\BannerBundle\Entity;

use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ImageBanner
 *
 * @package ADW\BannerBundle\Entity
 * @author Artur Vesker
 *
 * @ORM\Entity(repositoryClass="ADW\BannerBundle\Repository\DoctrineBannerRepository")
 */
class ImageBanner extends Banner
{

    /**
     * @var Media
     *
     * @ORM\OneToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     */
    protected $image;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $link;


    /**
     * Set image
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $image
     *
     * @return self
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

}
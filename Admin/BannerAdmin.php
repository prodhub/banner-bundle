<?php

namespace ADW\BannerBundle\Admin;

use ADW\BannerBundle\Entity\Banner;
use ADW\BannerBundle\Entity\HTMLBanner;
use ADW\BannerBundle\Entity\ImageBanner;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Controller\CRUDController;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Class BannerAdmin
 *
 * @author Artur Vesker
 */
class BannerAdmin extends Admin
{
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->setSubClasses([
            'image_banner' => 'ADW\BannerBundle\Entity\ImageBanner',
            'html_banner'  => 'ADW\BannerBundle\Entity\HTMLBanner'
        ]);
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('context', null, [], 'choice', [
                'choices' => $this->getContexts()
            ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('context', 'string')
            ->add('published')
        ;
    }

    /**
     * @param FormMapper $formMapper
     * TODO: blocks map
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('General')
                ->with(false)
                    ->add('name')
                    ->add('context', ChoiceType::class, [
                        'choices' => array_flip(array_map(function($value) {return $value['name'];}, $this->getContexts())), //Avoid optgroup autogeneration for nested array
                        'choices_as_values' => true,
                        'expanded' => true,
                    ])
                    ->add('publicationStartDate', 'datetime')
                    ->add('publicationEndDate', 'datetime')
                    ->add('published')
                ->end()
            ->end();

            if ($this->getSubject() instanceof ImageBanner) {
                $formMapper
                    ->tab('Изображение', ['link' => ['role' => 'banner-image-tab-link']])
                        ->with(false)
                            ->add('image', 'sonata_type_model_list',
                                ['required' => false],
                                [
                                    'link_parameters' => [
                                        'provider' => 'sonata.media.provider.image',
                                        'context' => 'banner',
                                    ],
                                ]
                            )
                            ->add('link', 'url', ['required' => false])
                        ->end()
                    ->end();
            }

            if ($this->getSubject() instanceof HTMLBanner) {
                $formMapper
                    ->tab('HTML код')
                        ->with(false)
                            ->add('html', 'textarea', ['label' => 'Code'])
                        ->end()
                    ->end()
                ;
            }
    }

    /**
     * @return mixed
     */
    private function getContexts()
    {
        return $this->getConfigurationPool()->getContainer()->getParameter('banner.contexts');
    }

    /**
     * @param mixed $object
     * @return string
     */
    public function toString($object)
    {
        return $object->getName() ?: 'Новый баннер';
    }


}
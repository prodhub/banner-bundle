<?php

namespace ADW\BannerBundle\Exception;

/**
 * Class ContextNotDefinedException
 *
 * @package ADW\BannerBundle\Exception
 * @author Artur Vesker
 */
class ContextNotDefinedException extends \Exception
{

    /**
     * @var string
     */
    protected $requestedContext;

    /**
     * @var array
     */
    protected $definedContexts;

    /**
     * ContextNotDefinedException constructor.
     * @param string $requestedContext
     * @param array $definedContexts
     */
    public function __construct($requestedContext, array $definedContexts = [])
    {
        $this->requestedContext = $requestedContext;
        $this->definedContexts = $definedContexts;
    }

    /**
     * @return string
     */
    public function getRequestedContext()
    {
        return $this->requestedContext;
    }

    /**
     * @return array
     */
    public function getDefinedContexts()
    {
        return $this->definedContexts;
    }
    
}
<?php

namespace ADW\BannerBundle\Repository;

use ADW\BannerBundle\Entity\Banner;

/**
 * Interface BannerRepositoryInterface
 *
 * @package ADW\BannerBundle\Repository
 * @author Artur Vesker
 *
 * TODO: banner interface
 */
interface BannerRepositoryInterface
{

    /**
     * @param string $context
     * @return Banner
     */
    public function findByContext($context);

}
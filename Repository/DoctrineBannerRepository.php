<?php


namespace ADW\BannerBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class DoctrineBannerRepository
 *
 * @package ADW\BannerBundle\Repository
 * @author Artur Vesker
 */
class DoctrineBannerRepository extends EntityRepository implements BannerRepositoryInterface
{

    /**
     * @inheritdoc
     */
    public function findByContext($context)
    {
        return $this->findBy(['context' => $context, 'published' => true]);
    }

}
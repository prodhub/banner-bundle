<?php

namespace ADW\BannerBundle\Block;

use ADW\BannerBundle\Exception\ContextNotDefinedException;
use ADW\BannerBundle\Repository\BannerRepositoryInterface;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BannerBlockService
 *
 * @package ADW\BannerBundle\Block
 * @author Artur Vesker
 */
class BannerBlockService extends BaseBlockService
{

    /**
     * @var BannerRepositoryInterface
     */
    protected $bannerRepository;

    /**
     * @var array
     */
    protected $contexts;

    /**
     * @var string
     */
    protected $defaultNotFoundTemplate;

    /**
     * BannerBlockService constructor.
     * @param string $name
     * @param EngineInterface $templating
     * @param BannerRepositoryInterface $bannerRepository
     * @param $contexts
     * @param $defaultNotFoundTemplate
     */
    public function __construct($name, EngineInterface $templating, BannerRepositoryInterface $bannerRepository, $contexts, $defaultNotFoundTemplate)
    {
        parent::__construct($name, $templating);
        $this->bannerRepository = $bannerRepository;
        $this->contexts = $contexts;
        $this->defaultNotFoundTemplate = $defaultNotFoundTemplate;
    }

    /**
     * @inheritdoc
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $context = $blockContext->getSetting('banner_context');

        if (!array_key_exists($context, $this->contexts)) {
            throw new ContextNotDefinedException($context, $this->contexts);
        }

        $config = $this->contexts[$context];

        $banners = $this->bannerRepository->findByContext($context);
        
        return $this->renderResponse($config['template'], [
            'block_context'  => $blockContext,
            'block'          => $blockContext->getBlock(),
            'banners' => $banners,
            'banner_context' => $context
        ], $response);
    }

    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired('banner_context')
        ;
    }

}